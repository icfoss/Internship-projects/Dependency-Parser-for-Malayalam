#!/usr/bin/python
# -*- coding: utf_8 -*-
import re
import unicodedata
import nltk
import codecs
import os
import sys
import pycrfsuite
import pickle
import csv
import pandas as pd
from PIL import ImageTk, Image

from flask import Flask, render_template, flash, request, url_for
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
reload(sys)
sys.setdefaultencoding('utf-8')
from nltk.tokenize import sent_tokenize, word_tokenize
sent=''
click='false'
 
#FLASK APP CONFIG
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
 
class ReusableForm(Form):
    name = TextField('Name:', validators=[validators.required()])

@app.route("/", methods=['GET', 'POST'])
def hello():
	form = ReusableForm(request.form)
 
	print form.errors
	if request.method == 'POST':
		name=request.form['name']
		print name

		#WRITE STRING TO FILE
		def input_string(stri):
			f1 = codecs.open("file1.txt", "w", "utf-8")
			f1.write(stri)
			f1.close()

		#WORDS INTO TOKENS
		def input_to_tokens():
			fdata	= codecs.open("file1.txt", "r",'utf-8','ignore')
			s=fdata.read()
			p = word_tokenize(s)
			length = len(p)
			print length	
			f1 = codecs.open("file2.txt", "w", "utf-8")
			for count in range(len(p)):
				f1.write(p[count])
				f1.write('\n')
			f1.close()	
			fdata.close()
			train1 = codecs.open("train_crf.txt", "r",'utf-8','ignore')
			train2 = list(train1)
	if form.validate():
		flash(name)
		input_string(name)
		input_to_tokens()

		#POS TAGGING WITH TNT TAGGER
		os.system('/home/nayana/components/tnt/tnt /home/nayana/components/tnt/newBIS file2.txt > test3')

		#CHUNK TAGGING WITH CRF++
		os.system('crf_test -m model_file test3 -o test4')
		lines = open('test4').readlines()
		lines1 = open('remove', 'w')
		lines1.writelines(lines[12:-3])	  
		lines1.close()
		with open("remove", "r") as in_file:
			buf = in_file.readlines()

		#FORMAT CONVERSION
		with open("malt.conll", "w") as out_file:
			i=1
			for line in buf:
					#line = 
				out_file.write(str(i)+'\t'+line.strip('\n')+'\t_\t_\t_\t_\t_\t_\n')
				i+=1
		
		#PARSING WITH MALT PARSER	
		os.system('java -jar maltparser-1.9.1.jar -c test2 -i malt.conll -o output.conll -m parse')
		with open('output.conll') as fin, open('output.csv', 'w') as fout:
			for line in fin:
				fout.write(line.replace('\t', ','))
		df1=pd.DataFrame()
		df1=pd.read_csv("output.csv",header=None)
		df1.columns = ['col1', 'col2','col3', 'col4','col5', 'col6','col7', 'col8','col9', 'col10']
		df1 = df1.drop(['col3','col4','col5','col6','col9','col10'], 1)
		df1.to_csv("output.csv")

		#CONVERSION TO A DIGRAPH FORMAT
		out_put = open('maltparser1.txt', 'w+')
		out_put.write('digraph{\n')
		for index, row in df1.iterrows():
			if row['col8']=="root":
				out_put.write('"O_ROOT"->"'+row['col2'].strip('\n')+'"[label="'+row['col8'].strip('\n')+'"];\n')
			for index, row2 in df1.iterrows():
				if row['col1']==row2['col7']:
					line1= row['col2']
					line2="->"
					line3=row2['col2']
					line4=row2['col8']
					print line1+line2+line3
					out_put.write('"'+line1.strip('\n')+'"->"'+line3.strip('\n')+'"[label="'+line4.strip('\n')+'"];\n')
		out_put.write('}')
		out_put.close()			
		os.system('dot -Tpng maltparser1.txt -o static/malt_tree8.png')
	return render_template('loginnn.html', form=form)
 
if __name__ == "__main__":
    app.run()
